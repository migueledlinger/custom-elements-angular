import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ce-angular';

  // usually not needed, only for functional demonstration
  currentState = '';
  serverState = "{\"selected\": \"\"}";
  serverData = '{\"text\": \"A week has seven days?\"}';

  // updates backend when user changes state of item
  updateServer(item: HTMLElement | null): void {
    if (item)
      this.serverState = item.getAttribute('state') ?? '';
  } 

  // updates item when changes comes from backend
  updateItem(item: HTMLElement | null, state_from_backend: string ): void {
    if (item)
      item.setAttribute('state', state_from_backend );
  }

  // update item 
  loadDataIntoItem(item: HTMLElement | null): void {
    if (item) 
    {
      item.setAttribute('state', this.serverState);
      item.setAttribute('data', this.serverData);
    }
  }


  ngOnInit(): void {

    console.log('onInit')
   
    const yesNoItem = document.getElementById("yes-no-item");

    // loading data from server
    this.loadDataIntoItem(yesNoItem);

     // event binding and interaction with yes-no-item 
    if (yesNoItem) {
      // on item state change update backend
      yesNoItem.addEventListener('stateChanged', () => {
        this.updateServer(yesNoItem);
      });
    }

    // on backend state change update item
    const stateChanger = document.getElementById('state-changer');
    if (stateChanger)
      stateChanger.addEventListener('input', (ev) => {

        try {
          // tests if it is valid json, outsource this to the item itself
          const json = JSON.parse((stateChanger as HTMLInputElement).value );
          this.serverState = JSON.stringify(json);

          this.loadDataIntoItem(yesNoItem);

      } catch {
          console.log("no json");
      }
      });
  }


}
