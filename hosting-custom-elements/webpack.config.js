// var path = require('path');

module.exports = {
    entry: './main.js',
    output: {
        filename: 'custom-elements.js'
    },
    resolve: {
        extensions: ['.json', '.js', '.jsx'],
        modules: ['node_modules'],
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    { loader: "style-shadow-dom-loader", options: { injectType: "shadowDOM", } }, 'css-loader'
                ],
            }
        ]
    }
};