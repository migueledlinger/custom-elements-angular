import {SliderItem } from './slider-item';
import { YesNoItem } from './yes-no-item';

customElements.define("slider-item", SliderItem);
customElements.define("yes-no-item", YesNoItem);